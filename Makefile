# Build the Docker image

# Docker image name (tag)
TAG=ipython:3.6

image: Dockerfile requirements.txt
	docker build -t $(TAG) .

custom-image: Dockerfile.custom requirements.txt
	docker build -t $(TAG)-custom --file Dockerfile.custom .
