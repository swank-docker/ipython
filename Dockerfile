# Use the official hub.docker.com Debian as parent
FROM debian:buster
#FROM python:3.6

USER root

# Install Packages
# ------------------------

# Install base python package through OS, and rest are through pip

RUN apt-get update && apt-get install -y \
    python3 \
    python3-pip \
    python3-tk \
    git \
    vim \
    sudo \
    libsm6 \
    && rm -rf /var/lib/apt/lists/*

# Cleanup
# NOTE:  official Debian images automatically run apt-get clean
#        explicit invocation not required if using official Debian image
# RUN apt-get clean -y

# Configure Environment & User
# -----------------------------
ENV APP_USER=kepler \
    APP_UID=1000 \
    APP_GID=100
ENV APP_HOME=/home/$APP_USER

# Create local user 
RUN useradd -m -s /bin/bash --no-log-init -r --uid $APP_UID $APP_USER && \
    chown -R $APP_USER:$APP_GID $APP_HOME && \
    chmod g+w /etc/passwd /etc/group

# Install requirements.txt packages 
# ---------------------------------
#
# REF: https://docs.docker.com/develop/develop-images/dockerfile_best-practices/#add-or-copy
#
# Add local files as late as possible to avoid cache busting
#USER $APP_UID

COPY requirements.txt /tmp/
RUN pip3 install --requirement /tmp/requirements.txt

COPY bin/run.sh /usr/local/bin/

# Container Config
# ---------------------------------
WORKDIR $APP_HOME/work

ENTRYPOINT ["run.sh"]

# Switch user to avoid accidental container runs as root
USER $APP_UID
