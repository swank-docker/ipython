#!/bin/bash

# Ref:  Based on content found in the jupyter notebook docker-stacks
#  https://github.com/jupyter/docker-stacks/blob/master/base-notebook/start.sh

set -e

# Exec the specified command or default to ipython3
if [ $# -eq 0 ]; then
    cmd=ipython3
else
    cmd=$*
fi


# Handle special flags if we're root
if [ $(id -u) == 0 ] ; then

    # Change UID of APP_USER to APP_UID if it does not match
    if [ "$APP_UID" != $(id -u $APP_USER) ] ; then
        echo "Set $APP_USER UID to: $APP_UID"
        usermod -u $APP_UID $APP_USER
    fi

    # Change GID of APP_USER to APP_GID if it does not match
    if [ "$APP_GID" != $(id -g $APP_USER) ] ; then
        echo "Set $APP_USER GID to: $APP_GID"
        groupmod -g $APP_GID -o $(id -g -n $APP_USER)
    fi

    # Enable sudo if requested
    if [[ "$GRANT_SUDO" == "1" || "$GRANT_SUDO" == 'yes' ]]; then
        echo "Granting $APP_USER sudo access"
        echo "$APP_USER ALL=(ALL) NOPASSWD:ALL" > /etc/sudoers.d/app
    fi

    # Exec the command as APP_USER with the PATH and the rest of
    # the environment preserved
    echo "Executing the command: $cmd"
    exec sudo -E -H -u $APP_USER PATH=$PATH $cmd

else

    if [[ "$APP_UID" == "$(id -u kepler)" && "$APP_GID" == "$(id -g kepler)" ]]; then
        # User is not attempting to override user/group via environment
        # variables, but they could still have overridden the uid/gid that
        # container runs as. Check that the user has an entry in the passwd
        # file and if not add an entry. Also add a group file entry if the
        # uid has its own distinct group but there is no entry.
        whoami &> /dev/null || STATUS=$? && true
        if [[ "$STATUS" != "0" ]]; then
            if [[ -w /etc/passwd ]]; then
                echo "Adding passwd file entry for $(id -u)"
                cat /etc/passwd | sed -e "s/^kepler:/relpek:/" > /tmp/passwd
                echo "kepler:x:$(id -u):$(id -g):,,,:/home/kepler:/bin/bash" >> /tmp/passwd
                cat /tmp/passwd > /etc/passwd
                rm /tmp/passwd
                id -G -n 2>/dev/null | grep -q -w $(id -u) || STATUS=$? && true
                if [[ "$STATUS" != "0" && "$(id -g)" == "0" ]]; then
                    echo "Adding group file entry for $(id -u)"
                    echo "kepler:x:$(id -u):" >> /etc/group
                fi
            else
                echo 'Container must be run with group "root" to update passwd file'
            fi
        fi

    else
        # Warn if looks like user want to override uid/gid but hasn't
        # run the container as root.
        if [[ ! -z "$APP_UID" && "$APP_UID" != "$(id -u)" ]]; then
            echo 'Container must be run as root to set $APP_UID'
        fi
        if [[ ! -z "$APP_GID" && "$APP_GID" != "$(id -g)" ]]; then
            echo 'Container must be run as root to set $APP_GID'
        fi
    fi

    # Execute the command
    echo "Executing the command: $cmd"
    exec $cmd

fi
