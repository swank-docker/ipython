# Dockerfile to build ipython image for development testing

## Build:
Build the docker image using:

```
docker build -t <TAG> .

```
where `<TAG>`  is the tagname for the docker image.

A Makefile is provided to simplify the process of building.

## Customization:

The python packages are installed using pip into the docker image.
In order to customize the packages available, edit the 
`requirements.txt` file prior to building the image.


## Run:

Run the docker image in interactive mode using one of the following options (Tested on Linux):

1. Run the image in interactive mode, remove container on exit:
```
# Run the image, for default ipython3
docker run --rm -it <IMAGE NAME>
```

1. Run the image in interactive mode, remove the container on exit.  Additionally, __mount the present working directory to a mount point within the container__.  The `--user` argument and environment variables `APP_UID` and `APP_GID` is passed to the docker run so that any files modified or created inside the container in the mounted director from the host will have the proper user:group credentials. 
```
# Run and mount working directory
docker run --rm --tty --interactive  \
     --user root  \
     -e APP_UID=`id -u` -e APP_GID=`id -g`
     --mount type=bind,source="$PWD",target=/home/<USERNAME>/<MOUNT POINT> \
     <IMAGE NAME>
``` 

1. Run the image in interactive mode, remove the container on exit.
 Mount the present working directory to a mount point
 within the container.
 Additionally, __share the host X11 socket to allow graphics__. 
```
# Run and share the X11 socket for display
docker run --rm --tty --interactive \
     --user root  \
     -e APP_UID=`id -u` -e APP_GID=`id -g`
     -e DISPLAY=$DISPLAY \
     -v /tmp/.X11-unix:/tmp/.X11-unix \
     --mount type=bind,source="$PWD",target=/home/<USERNAME>/<MOUNT POINT> \
     <IMAGE NAME>
``` 

1. Additionally, __mount the .ssh directory to share credentials for git__.  Note that the .ssh directory is mounted in readonly mode.  It is recommended to run git outside of the container, yet this allows git access if needed. Note, 
if you have a ~/.gitconfig file that will need to be generated as well.
```
docker run --rm --tty --interactive \
     --user root  \
     -e APP_UID=`id -u` -e APP_GID=`id -g`
     -e DISPLAY=$DISPLAY \
     -v /tmp/.X11-unix:/tmp/.X11-unix \
     --mount type=bind,source=/home/<USERNAME>/.ssh,target=/home/<USERNAME>/.ssh,readonly 
     --mount type=bind,source="$PWD",target=/home/<USERNAME>/<MOUNT POINT> \
     <IMAGE NAME>
``` 

### Mac OS

While I do not have access to Mac OS to verify, the following should work for the graphics on X11.

For the X11 forwarding on Mac OS, a modification is necessary for the X11 graphics.  XQuartz X11 server is needed.  Install and run the XQuartz X11 server first.  Command line startup is ```open -a XQuartz```. In the preferences for XQuartz, and Security tab, enable "Allow connections from network clients".  Add the ip to the authorized connection list using ```/usr/X11/bin/xhost + 127.0.0.1```.
For the display environment on the docker run command, replace with ```DISPLAY=127.0.0.1:0```

## Note:


The Dockerfile uses the official hub.docker.com Debian as parent.

At the time of this writing, the following was observed:

If use python:3.6 as parent, `dpkg -l | grep python3` shows python version 3.5, but 
`python --version` returns version 3.6.5.

If use debian:buster as parent and `apt-get install python3`, 
`dpkg -l | grep python` shows python version 3.6.5.

### Image Size:

The size of the debian:testing (buster) image is 1 Gig, and the size of the 
python image is 1.5 G.



## Alternatives

### Customization to Dockerhub image

If you just want to add packages via pip to the already built
ipython image available on docker hub, use the Dockerfile.custom and 
your custom `requirements.txt` file to build an image.
For example, 

1. Create your 'requirements.txt' file listing desired packages.

1. Build image
```
docker build -t ipython:3.6-custom --file Dockerfile.custom .
--or--
make custom-image
```

When executing the docker run command, use your custom image name, ie ipython:3.6-custom

### Jupyter

If you need access to Jupyter documents, consider using the various 
jupyter images available on docker hub:  
http://jupyter-docker-stacks.readthedocs.io/en/latest/using/selecting.html

For example, a scipy notebook is found at:  
https://hub.docker.com/r/jupyter/scipy-notebook/
